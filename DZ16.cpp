﻿// DZ16.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>



//const int MAX_SIZE = 10;
//
//class stack 
//{
//    int m_size;            // Количество элементов в стеке
//    int m_elems[MAX_SIZE]; // Массив для хранения элементов
//    void push( int d);           // Добавить в стек новый элемент
//    int pop();             // Удалить из стека последний элемент
//                           // и вернуть его значение
//    int size();            // Вернуть количество элементов в стеке
//};
 class Stack 
{
  int* array;
  int size;
  int last_index;
    
public:
  Stack()
    {
        last_index = -1;
        size = 10;
        array = new int[size];
    }
    void Push(int new_elem)
    {
        /*if (last_index == size)
            ++last_index;*/
        if (++last_index < size)
        {
            array[last_index] = new_elem; 
        }
    }
    int Pop()
    {
        /*if (last_index < size)
            int new_elem(size >> 1);*/
        if (last_index == -1)
            return INT_MAX;
        return array[last_index--];
    }
    ~Stack()
    {
        delete[] array;
    }
};
    
    int main()
    {
        Stack stack_object;

        stack_object.Push(5);
        stack_object.Push(2);
        stack_object.Push(8);
        std::cout << stack_object.Pop() << std::endl;
        std::cout << stack_object.Pop() << std::endl;
    }